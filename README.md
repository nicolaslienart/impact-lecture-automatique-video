# Mesure de l'impact énergétique sur la machine client pour la lecture automatique d'une vidéo

## Usage

Nécessite une machine GNU/Linux avec turbostat d'installé, fonctionne avec CPU Intel >= à la générarion Caby Lake

	1. Installer turbostat (linux-cpupower sur Debian)
	2. Se placer à la racine de ce dépot et éxecuter le script `./script.sh <minuteur (voir manuel de timeout)> [<titre de la mesure>]`

## Rapport *Écologie du Digital - État des lieux et démarches de sobriété*

Ce dispositif a permis d'obtenir des statistiques approximatifs pour le rapport *Écologie du Digital - État des lieux et démarches de sobriété* mené par Nicolas Liénart.

[Dépot du rapport mis à jour](https://framagit.org/nicolaslienart/rapport-eco-num-web)

## Objectif

Mesurer la différence de consommation énergétique causée par la lecture automatique d'une vidéo sur une page web.

## Dispositif

L'idée est d'isoler le test afin de réduire des perturbations extérieures à ce test. On procède donc au rendu d'une page HTML locale très simple contenant la vidéo (elle-même locale), une version de la page en lecture automatique et l'autre non (grâce à l'attribut `autoplay`).

La vidéo provient du site Marmiton.org qui se trouvait sur une page de recette, mise en avant sur la page d'accueil qui a potentiellement de nombreuses visites. Cette vidéo était en lecture automatique pour illustrer la recette mais n'était pas nécessaire du fait des explications écrites. Elle était disponible en deux versions qui sont SD et HD et j'ai pris la première qui constitue un minimum (on peut faire pire).

J'utilise une version fraiche de Firefox (79.0) sans extension qui pourrait interferrer. Le seul réglage est désactiver le session storage pour empêcher les onglets de se restaurer automatiquement.

## Mesure

La mesure consiste à ouvrir Firefox à l'URL du fichier HTML contenant la vidéo en lecture automatique, de rafraichir la page à chaque fois que la vidéo a été lue. La consommation énergétique est mesurée pendant toute la durée du test au moyen de la commande `turbostat` et pendant 5 minute avant de couper la mesure. Puis faire de même avec l'autre version de la page sans lecture de la vidéo.

## Résultats

Puissance en moyenne :
 - avec lecture : 3,83 W
 - sans lecture : 2,01 W

Soit une différence de 1.82 W, presque 2 fois plus d'énergie !

## Interprétation et conséquences

On peut calculer l'énergie en joules consommée comme ceci en sachant que la vidéo dure 27 secondes :

1,82 * 27 = 49,14

Imaginons que nous avons une centaine de visiteurs par jour sur cette page qui ne sont pas intéressés par la vidéo. On en vient à cette qantitée gaspillée :

49,14 * 100 * 365 = 1.793.610 joules

Qu'aurions-nous pu faire de cette énergie si elle n'était pas gaspillée, je vais prendre comme exemple la charge complète de mon smartphone qui a une capacité de 11,6 Watt-heure. Elle contient donc 

11,6 * 3600 (une heure en secondes) = 41.760 joules

On peut donc déduire combien de charges complètes ces 1.793.610 joules aurait pu soutenir

1.793.610 / 41.760 = 42,9 charges

Ca n'est pas négligeable, et cette énergie n'est peut-être pas utilisée à bon escient ! 

## Conclusion

Ce test est minimal, il ne prend pas en compte l'énergie consommée pour le stockage et le transfert de cette vidéo depuis les serveurs de Marmiton.org, d'autant plus que c'est la version la plus basse qui a été testée (normalement pour être lue sur un réseau à faible bande passante sans latence).

On peut se rendre compte à quel point certains designs peuvent entrainer de lourdes conséquences (il s'agit d'un simple attribut à changer ici).
