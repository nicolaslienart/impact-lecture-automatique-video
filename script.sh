#!/bin/sh

run_benchmark() {
	local HTML_FILE=$1
	#echo "sudo turbostat --quiet --Summary --out $TEMP_FILE timeout --signal=SIGINT $TIMEOUT sudo -u nicolas $FIREFOX_EXECUTABLE --safe-mode --new-instance $HTML_FILE"
	sudo turbostat --quiet --Summary --out $TEMP_FILE timeout --signal=SIGINT $TIMEOUT sudo -u nicolas $FIREFOX_EXECUTABLE --profile ./profile --new-instance $HTML_FILE
}

generate_filename() {
	local CURRENT_PATH=$(dirname $1)
	local DATE=$(date +%Y-%m-%d_%H-%M-%S)
	if [ "$2" ]
	then
		local FILENAME=$(echo $CURRENT_PATH'/Results_'$2'-'$DATE'.txt')
	else
		local FILENAME=$(echo $CURRENT_PATH'/Results_'$DATE'.txt')
	fi
	echo $FILENAME
}

main() {
	FIREFOX_EXECUTABLE=/home/nicolas/Downloads/Apps/firefox/firefox
	TEMP_FILE=temp.txt
	TIMEOUT=$1
	FILENAME=$(generate_filename $0 $2)
	echo "Power consumption of Firefox running for $TIMEOUT" > $FILENAME
	echo >> $FILENAME
	echo "Benchmark with ./video_autoplay.html" >> $FILENAME
	run_benchmark ./video_autoplay.html
	cat $TEMP_FILE >> $FILENAME
	echo >> $FILENAME
	echo "Benchmark with ./video_no_autoplay.html" >> $FILENAME
	run_benchmark ./video_no_autoplay.html
	cat $TEMP_FILE >> $FILENAME
}

#NOTE usage: ./script.sh <timeout in seconds> [ <title> ]

main $@
